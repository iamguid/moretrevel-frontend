var fs = require('fs');
var path = require('path');

var gulp = require('gulp');

// Load all gulp plugins automatically
// and attach them to the `plugins` object
var plugins = require('gulp-load-plugins')();

// Temporary solution until gulp 4
// https://github.com/gulpjs/gulp/issues/355
var runSequence = require('run-sequence');

var node_config = require('./package.json');
var bower_config = require('./bower.json');
var project = require('./project.json');

var dirs = project['configs'].directories;

// ---------------------------------------------------------------------
// | Helper tasks                                                      |
// ---------------------------------------------------------------------

gulp.task('archive:create_archive_dir', function () {
    fs.mkdirSync(path.resolve(dirs.archive), '0755');
});

gulp.task('archive:zip', function (done) {
  var archiveName = path.resolve(dirs.archive, project.name + '_v' + project.version + '.zip');
  var archiver = require('archiver')('zip');
  var files = require('glob').sync('**/*.*', {
    'cwd': dirs.dist,
    'dot': true // include hidden files
  });
  var output = fs.createWriteStream(archiveName);

  archiver.on('error', function (error) {
    done();
    throw error;
  });

  output.on('close', done);

  files.forEach(function (file) {
    var filePath = path.resolve(dirs.dist, file);

    // `archiver.bulk` does not maintain the file
    // permissions, so we need to add files individually
    archiver.append(fs.createReadStream(filePath), {
      'name': file,
      'mode': fs.statSync(filePath)
    });

  });

  archiver.pipe(output);
  archiver.finalize();
});

gulp.task('clean', function (done) {
  require('del')([
    dirs.dist
  ], done);
});


gulp.task('setGlobs', function () {
  return gulp.src([

    // Replace files path
    dirs.dist + '/css/main.css',
    dirs.dist + '/*.html',

  ], {

    // Include hidden files by default
    dot: false

  })
  .pipe(plugins.replace(/{{IMG_PATH}}/g, '/img'))
  .pipe(gulp.dest(dirs.dist));
});


gulp.task('compile', [
  'compile:jade',
  'compile:less'
], function () {
  gulp.run('setGlobs');
});

gulp.task('compile:jade', function () {
  var jquery_version = require('./bower_components/jquery/bower.json').version;
  var config = require(dirs.src + '/jade/variables.json');

  return gulp.src(dirs.src + '/jade/pages/*.jade')
    .pipe(plugins.jade({
      locals: config,
      pretty: true
    }))
    .pipe(plugins.replace(/{{JQUERY_VERSION}}/g, jquery_version))
    .pipe(gulp.dest(dirs.dist));
});

gulp.task('compile:less', function () {
  return gulp.src(dirs.src + '/less/main.less')
    .pipe(plugins.less({
      paths: [ dirs.src + '/less/**/*.less' ]
    }))
    .pipe(plugins.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 8', '> 1%'],
      cascade: false
    }))
    .pipe(gulp.dest(dirs.dist + '/css'));
});


gulp.task('watch', [
  'watch:less',
  'watch:jade'
]);

gulp.task('watch:less', function () {
  gulp.watch(dirs.src + '/less/**', function() {
    gulp.run('compile');
  });
});

gulp.task('watch:jade', function () {
  gulp.watch(dirs.src + '/jade/**/*.jade', function() {
    gulp.run('compile');
  });
});


gulp.task('copy', [
  'copy:modernizr.js',
  'copy:jquery.js',
  'copy:normalize.css',
  'copy:misc'
]);

gulp.task('copy:modernizr.js', function () {
    var dir = './bower_components/modernizr';
    return gulp.src(dir + '/modernizr.js')
      .pipe(plugins.uglify())
      .pipe(plugins.rename({
        suffix: ".min"
      }))
      .pipe(gulp.dest(dirs.dist + '/js/vendor'));
});

gulp.task('copy:jquery.js', function () {
    var dir = './bower_components/jquery';
    return gulp.src(dir + '/dist/jquery.min.js')
      .pipe(gulp.dest(dirs.dist + '/js/vendor'));
});

gulp.task('copy:normalize.css', function () {
  var dir = './bower_components/normalize.css';
  return gulp.src(dir + '/normalize.css')
    .pipe(gulp.dest(dirs.dist + '/css'));
});

gulp.task('copy:misc', function () {
  return gulp.src([

    // Copy all files
    dirs.src + '/**/*',

    // Exclude the following files
    // (other tasks will handle the copying of these files)
    '!' + dirs.src + '/less/**/',
    '!' + dirs.src + '/less/',

    '!' + dirs.src + '/jade/**/',
    '!' + dirs.src + '/jade/',
  ], {

    // Include hidden files by default
    dot: true

  }).pipe(gulp.dest(dirs.dist));
});


// gulp.task('lint:js', function () {
//   return gulp.src([
//     'gulpfile.js',
//     dirs.src + '/js/*.js',
//   ])//.pipe(plugins.jscs())
//     .pipe(plugins.jshint())
//     .pipe(plugins.jshint.reporter('jshint-stylish'))
//     .pipe(plugins.jshint.reporter('fail'));
// });


// ---------------------------------------------------------------------
// | Main tasks                                                        |
// ---------------------------------------------------------------------

gulp.task('archive', function (done) {
  runSequence(
    'build',
    'archive:create_archive_dir',
    'archive:zip',
  done);
});

gulp.task('build', function (done) {
  runSequence(
    // ['clean', 'lint'],
    ['clean'],
    ['copy', 'compile'],
  done);
});

gulp.task('default', ['build']);
